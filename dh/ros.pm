# Copyright 2021-2022 Timo Röhling <roehling@debian.org>
# SPDX-License-Identifier: MIT
package Debian::Debhelper::Buildsystem::ros;

use strict;
use Debian::Debhelper::Dh_Lib qw(doit);
use base 'Debian::Debhelper::Buildsystem';

sub DESCRIPTION {
    "ROS package builder"
}

sub check_auto_buildable {
    my $this = shift;
    return doit('rosbuilder', '--detect', '--sourcedir', $this->get_sourcedir());
}

sub new {
    my $class = shift;
    my $this = $class->SUPER::new(@_);
    $this->prefer_out_of_source_building();
    return $this;
}

sub configure {
    return 1
}

sub build {
    return 1
}

sub test {
    return 1
}

sub install {
    my $this = shift;
    my $destdir = shift;
    return doit('rosbuilder', '--sourcedir', $this->get_sourcedir(), '--builddir', $this->get_builddir(), '--destdir', $destdir, @_);
}

sub clean {
    my $this = shift;
    doit('rosbuilder', '--clean', '--sourcedir', $this->get_sourcedir(), '--builddir', $this->get_builddir(), @_);
    doit('rm', '-rf', $this->get_builddir());
    doit('rm', '-rf', '.pybuild', '.pytest_cache');
}

1
