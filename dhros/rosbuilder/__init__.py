# Copyright 2021-2022 Timo Röhling <roehling@debian.org>
# SPDX-License-Identifier: MIT
from .rosbuilder import main  # type: ignore[reportUnusedImport]
