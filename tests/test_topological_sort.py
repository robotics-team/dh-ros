# Copyright 2021-2022 Timo Röhling <roehling@debian.org>
# SPDX-License-Identifier: MIT
import pytest

from dhros.common import topological_sort


def test_total_order_dag():
    assert topological_sort({1: [2], 2: [3], 3: []}) == [3, 2, 1]


def test_partial_order_dag():
    assert topological_sort({1: [3], 2: [3], 3: [], 4: [6], 5: [6], 6: []}) == [
        3,
        6,
        1,
        2,
        4,
        5,
    ]


def test_unknown_depends():
    assert topological_sort({1: [2, 3, 4, 5, 6, 7, 8], 2: [3, 4, 5, 6, 7, 8]}) == [2, 1]


def test_dependency_loops():
    with pytest.raises(RuntimeError, match="loop"):
        topological_sort({1: [1]})
    with pytest.raises(RuntimeError, match="loop"):
        topological_sort({1: [2], 2: [1]})
    with pytest.raises(RuntimeError, match="loop"):
        topological_sort({1: [2], 2: [3], 3: [1]})
