# Copyright 2021-2022 Timo Röhling <roehling@debian.org>
# SPDX-License-Identifier: MIT
import pytest
import os
import pathlib
from catkin_pkg.package import Package, Person, License, Dependency, Export
from dhros.rosbuilder.rosbuilder import prepare_argparse

FAKE_PACKAGES = [
    (
        pathlib.Path("foo_ament_cmake"),
        Package(
            name="foo_ament_cmake",
            version="1.0.0",
            description="fake ament_cmake package",
            licenses=[License("Apache-2.0")],
            maintainers=[Person("Timo Röhling", "roehling@debian.org")],
            exports=[Export("build_type", "ament_cmake")],
        ),
    ),
    (
        pathlib.Path("foo_ament_python"),
        Package(
            name="foo_ament_python",
            version="1.0.0",
            description="fake ament_cmake package",
            licenses=[License("Apache-2.0")],
            maintainers=[Person("Timo Röhling", "roehling@debian.org")],
            exports=[Export("build_type", "ament_python")],
        ),
    ),
    (
        pathlib.Path("foo_catkin"),
        Package(
            name="foo_catkin",
            version="1.0.0",
            description="fake catkin package",
            licenses=[License("Apache-2.0")],
            maintainers=[Person("Timo Röhling", "roehling@debian.org")],
            exports=[Export("build_type", "catkin")],
        ),
    ),
    (
        pathlib.Path("foo_cmake"),
        Package(
            name="foo_cmake",
            version="1.0.0",
            description="fake catkin package",
            licenses=[License("Apache-2.0")],
            maintainers=[Person("Timo Röhling", "roehling@debian.org")],
            exports=[Export("build_type", "cmake")],
        ),
    ),
]


@pytest.fixture
def fake_packages(mocker):
    mocker.patch("dhros.common.find_packages", lambda _1, _2: FAKE_PACKAGES)
    yield


@pytest.fixture
def rosbuilder(fake_packages):
    p = prepare_argparse()

    def result(*args):
        return p.parse_args(["--dry-run"] + list(args))

    return result
